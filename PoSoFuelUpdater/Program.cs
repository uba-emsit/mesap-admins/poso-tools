﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using M4DBO;
using PoSo;

namespace PoSoFuelUpdater
{
    class Program
    {
        static int YEAR_TO_UPDATE = 2021;
        static int POINTSOURCE_DIMENSION = 8;

        static string WORK_FOLDER = "C:\\Users\\hausmann\\Entwicklung\\Visual Studio Workspace\\Projects\\PoSoFuelUpdater\\";
        static string DATA_FILE = WORK_FOLDER + "data.csv";
        static string FUEL_FILE = WORK_FOLDER + "fuels.csv";
        static string SQL_SCRIPT = WORK_FOLDER + "script.sql";
        static string PROBLEM_LOG = WORK_FOLDER + "failed.log";
        static char DELIMITER = ';';

        static string SCRIPT_PREFIX = "SET DATEFORMAT ymd;" + Environment.NewLine;

        static void Main(string[] args)
        {
            Console.WriteLine("This is the fuel update program!");
            Console.WriteLine("Press any key to connect to default database!");
            Console.ReadKey();

            Database poso = new Database();
            poso.Connect("Fuel Update");
            Console.WriteLine("Connected!");

            Console.WriteLine("Reading fuel map...");
            Dictionary<string, int> fuels = new Dictionary<string, int>();
            foreach (string fuel in File.ReadAllLines(FUEL_FILE).Skip(1))
                fuels.Add(fuel.Split(DELIMITER)[1], Int32.Parse(fuel.Split(DELIMITER)[0]));
            Console.WriteLine("Found " + fuels.Count + " fuels...");

            Console.WriteLine("Reading data...");
            File.WriteAllText(SQL_SCRIPT, SCRIPT_PREFIX);
            File.WriteAllText(PROBLEM_LOG, "");
            int index = 2100000; // Make sure this is high enough before importing, editing PoSo uses up IDs as well!
            foreach (string line in File.ReadAllLines(DATA_FILE).Skip(1))
            {
                string[] details = line.Split(DELIMITER);

                dboTreeObject descriptor = poso.db.TreeObjects[details[0], POINTSOURCE_DIMENSION];
                MyPointSource source = new MyPointSource(poso.db, descriptor.ObjNr);
                int[] fuelReferences = CreateFuelReferences(fuels, details[2]);

                Console.WriteLine(source.GetId());
                Console.WriteLine(source.GetName());
                //source.ListFuels(false);

                try
                {
                    source.CreateSQL(SQL_SCRIPT, YEAR_TO_UPDATE, fuelReferences, index);
                }
                catch (Exception ex)
                {
                    File.AppendAllText(PROBLEM_LOG, source.GetId() + " " + source.GetName() + " " + ex.Message + "\n");
                }
                index += fuelReferences.Length;
                Console.WriteLine("------------------------------");
            }

            Console.WriteLine("Done!");
            Console.WriteLine("Press any key to disconnect and close!");
            Console.ReadKey();

            poso.Disconnect();
        }

        private static int[] CreateFuelReferences(Dictionary<string, int> fuelDirectory, string list)
        {
            List<int> result = new List<int>();
            string[] fuels = list.Split('\\');

            foreach (string fuel in fuels)
            {
                if (!fuelDirectory.ContainsKey(fuel))
                    throw new Exception("Unknown fuel! " + fuel);
                else
                    result.Add(fuelDirectory[fuel]);
            }

            return result.ToArray();
        }
    }

    class MyPointSource : PointSource
    {
        public MyPointSource(dboDatabase database, int key) : base(database, key) {}

        private static int TECHNOLOGY_COMPONENT = 47;
        private static int FUEL_FIELD = 330;

        static string UPDATE_TEMPLATE = "UPDATE [dbo].[AnnexItemData] SET [ValidToDate] = N'{0}-12-31 00:00:00', [IsHistory] = -1, [ChangeDate] = N'2022-06-14 10:10:35' WHERE [ItemDataNr] = {1};" + Environment.NewLine;
        static string INSERT_TEMPLATE = "INSERT INTO [dbo].[AnnexItemData] ([ItemDataNr], [AnnexSetNr], [AnnexType], [AnnexSetLinkNr], [ItemNr], [ItemType], [ComponentNr], [TextData], [NumberData], [ReferenceData], [CalendarData], [FileName], [ValidFromDate], [ValidToDate], [ChangeID], [ChangeDate], [Origin], [IsHistory], [IsPoolObj], [IsDeleted])" + Environment.NewLine
            + "VALUES ({0}, {1}, 1, {1}, 330, 16, 47, N'', NULL, {2}, N'1899-12-30 00:00:00', N'', N'{3}-01-01 00:00:00', N'9999-12-31 00:00:00', N'Kludt_Admin', N'2023-06-14 10:10:35', 3, 0, 0, 0);" + Environment.NewLine;

        public void ListFuels(bool includeHistory)
        {
            IEnumerator linksEnum = annexSetLinks.GetEnumerator();
            while (linksEnum.MoveNext())
            {
                dboAnnexSetLink link = linksEnum.Current as dboAnnexSetLink;

                if (link.ComponentNr == TECHNOLOGY_COMPONENT)
                {
                    // Find current values
                    dboAnnexItemDatas datas = db.CreateObject_AnnexItemDatas("");
                    datas.DbReadByItemNr(link.AnnexSetNr, TECHNOLOGY_COMPONENT, FUEL_FIELD, false, link.AnnexSetLinkNr, true);
                    datas.DbReadByItemNr(link.AnnexSetNr, TECHNOLOGY_COMPONENT, FUEL_FIELD, includeHistory, link.AnnexSetLinkNr, false);

                    IEnumerator dataEnum = datas.GetEnumerator();
                    while (dataEnum.MoveNext())
                    {
                        Console.WriteLine(link.AnnexSetNr);
                        Console.WriteLine((dataEnum.Current as dboAnnexItemData).ReferenceData + " from " +
                            (dataEnum.Current as dboAnnexItemData).ValidFromDate + " to " + (dataEnum.Current as dboAnnexItemData).ValidToDate);
                    }
                }
            }
        }

        public void CreateSQL(string file, int year, int[] fuelReferences, int index)
        {
            IEnumerator linksEnum = annexSetLinks.GetEnumerator();
            
            while (linksEnum.MoveNext())
            {
                dboAnnexSetLink link = linksEnum.Current as dboAnnexSetLink;
                
                if (link.ComponentNr == TECHNOLOGY_COMPONENT)
                {
                    // Find current values
                    dboAnnexItemDatas datas = db.CreateObject_AnnexItemDatas("");
                    datas.DbReadByItemNr(link.AnnexSetNr, TECHNOLOGY_COMPONENT, FUEL_FIELD, false, link.AnnexSetLinkNr, true);

                    if (year < 1901 || year > 9999)
                        // This should never happen
                        throw new Exception("Invalid year given!");
                    else if (fuelReferences.Length == 0)
                        // This should never happen
                        throw new Exception("No fuel references given!");
                    else
                    {
                        // Update current data "valid to dates"
                        IEnumerator dataEnum = datas.GetEnumerator();
                        while (dataEnum.MoveNext())
                        {
                            dboAnnexItemData data = dataEnum.Current as dboAnnexItemData;
                            File.AppendAllText(file, String.Format(UPDATE_TEMPLATE, year - 1, data.ItemDataNr));

                            /*int handle = db.Root.GetFreeLockHandle();

                            
                            DateTime old = data.ValidToDate;

                            mspEnableModifyResultEnum result = data.EnableModify(handle);
                            if (result == mspEnableModifyResultEnum.mspEnableModifySuccess)
                            {
                                data.ValidToDate = new DateTime(year - 1, 12, 31);
                                
                                // Write to database
                                datas.DbUpdateAll(handle);
                                data.DisableModify(handle);
                            }
                            else
                                throw new Exception("Cannot modify point source! " + GetName());*/
                        }

                        // Add new values
                        foreach (int reference in fuelReferences)
                        {
                            File.AppendAllText(file, String.Format(INSERT_TEMPLATE, index++ , link.AnnexSetNr, reference, year));

                            /*int handle = db.Root.GetFreeLockHandle();
                            dboAnnexItemData data = datas.Add(handle, mspAnnexTypeEnum.mspAnnexTypeMD, link.AnnexSetNr, link.AnnexSetLinkNr,
                                false, FUEL_FIELD, mspAnnexItemTypeEnum.mspAnnexItemTypeMultiDescriptor, TECHNOLOGY_COMPONENT);

                            data.ReferenceData = reference;
                            data.ValidFromDate = new DateTime(year, 1, 1);
                            
                            // Write to database
                            datas.DbUpdateAll(handle);
                            data.DisableModify(handle);*/
                        }                        
                    }
                }
            }
        }
    }
}
