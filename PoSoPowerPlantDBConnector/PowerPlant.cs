﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PoSo;

namespace PoSoPowerPlantDBConnector
{
    class PowerPlant
    {
        private static char separator = ';';
                
        public PowerPlant(String data)
        {
            this.Name = data.Split(separator)[0];
            this.Operator = data.Split(separator)[1];
            this.State = (State)Enum.Parse(typeof(State), data.Split(separator)[3]);
            this.StartYear = data.Split(separator)[15];
            this.Renovation = data.Split(separator)[16];
            this.Status = data.Split(separator)[14];
            this.Technique = data.Split(separator)[19];
        }

        public String Name { get; set; }
        
        public String Operator { get; set; }

        public State State{ get; set; }

        public String Status { get; set; }

        public String StateName
        {
            get
            {
                return State.LongName();
            }
            set
            {
                ;
            }
        }

        public String StartYear { get; set; }

        public String Renovation { get; set; }

        public String Technique { get; set; }
    }
}
