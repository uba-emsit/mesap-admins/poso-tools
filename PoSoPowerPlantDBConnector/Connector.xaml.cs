﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using PoSo;
using M4DBO;
using System.IO;

namespace PoSoPowerPlantDBConnector
{
    public partial class Connector : Application
    {
        private static String PROGRAM_NAME = "PoSo Power Plant Database Connector";
        private static String POWERPLANT_FILE = "C:\\Dokumente und Einstellungen\\hausmann\\Eigene Dateien\\Visual Studio 2008\\Projects\\PoSoPowerPlantDBConnector\\kraftwerksdatenbank.csv";
        private static String COLLECTIVE_ID = "PoSo_KML_Converter";

        private Database poso = new Database();
        internal HashSet<PowerPlant> powerPlants = new HashSet<PowerPlant>();
        internal HashSet<PointSource> pointSources = new HashSet<PointSource>();
        
        protected override void OnStartup(StartupEventArgs e)
        {
            poso.Connect(PROGRAM_NAME);
            
            ReadPowerPlants();

            dboList list = Program.CreatePointSourceList(poso, COLLECTIVE_ID);
            for (int count = 1; count <= list.Count; count++)
            {
                PointSource pointSource = new PointSource(poso.db, list[count].ToString());
                pointSources.Add(pointSource);
            }
                        
            base.OnStartup(e);
        }

        protected override void OnExit(ExitEventArgs e)
        {
            poso.Disconnect();

            base.OnExit(e);
        }

        private void ReadPowerPlants()
        {
            Console.WriteLine("Start reading power plant data from " + POWERPLANT_FILE);

            TextReader reader = new StreamReader(POWERPLANT_FILE, true);
            String line = reader.ReadLine();
            // Skip header
            line = reader.ReadLine();

            while (line != null && line.Length > 50)
            {
                PowerPlant plant = new PowerPlant(line);
                powerPlants.Add(plant);

                // Console.WriteLine("Found plant \"" + plant.Name + "\" in " + plant.State.LongName());

                line = reader.ReadLine();
            }

            reader.Close();

            Console.WriteLine(powerPlants.Count + " power plant(s) found.");
        }
    }
}
